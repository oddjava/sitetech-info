package company;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/* This code is find side-wide technologies of companies website and add those technologies to DB.
 * */
public class MainProgramSiteTech 
{
	// References related to Mongo Database.
	MongoClient mongoClient;
	DB company;
	DBCollection parsedDataForVPS;
		
	// Set Timer
	static long startTime = System.currentTimeMillis();
	
	// Constructor to establish Mongo connection
	MainProgramSiteTech(String ip)
	{
		mongoClient = new MongoClient(ip, 27017);
		company = mongoClient.getDB("company");
		parsedDataForVPS=company.getCollection("parsedDataForVPS");		
	}
	
	// Main Method
	public static void main(String args[]) throws Exception 
	{	
		/* args[0] = IP Address
		 * args[1] = Start ID
		 * args[2] = End ID
		 * */
		MainProgramSiteTech obj=new MainProgramSiteTech(args[0]);
		obj.getSiteTechInfo(Integer.parseInt(args[1]),Integer.parseInt(args[2]));
		System.exit(0); // Send status 0 to shell script indicating that normal termination of code.
	}

	// This method 
	void getSiteTechInfo(int startid,int endid) 
	{			
		// Get document from DB whose sitetechstatus is false				
		DBCursor cursor = parsedDataForVPS.find(new BasicDBObject("_id", new BasicDBObject("$lte", endid).append("$gte", startid)).append("sellerStatus" , "validDomain").append("sitetechStatus" , "false"));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		
		int cursorCount=cursor.count();
		System.out.println("Total Count="+cursorCount);
		
		DBObject present=null;
		String link;
		String id;
		double loads[];	
		
		while (cursor.hasNext()) 
		{
			loads=getLoadAvg();
			if(loads[0]<1.50) // Check load Average < 1.5 
			{
				present=cursor.next();
				link="";
				id="";
				
				link = present.get("link").toString();
				id=	present.get("_id").toString();
				
				callThread(id,link,parsedDataForVPS);	
											
		    }
			else // When cpu load average exceed 1.5 code will get terminate.
			{
				System.out.println("So tired. I'm Shuting Nowwwwwww........"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
				System.exit(123);	// return status 123 to shell script indicating abnormal termination.			
			}
		}		
		
		long endTime = System.currentTimeMillis();
		long minutes = TimeUnit.MILLISECONDS.toMinutes(endTime - startTime);
		System.out.println("Total Count="+cursorCount);
		System.out.println("It tooks " + minutes + " min");
		
		
	}
	
	// This method will wait for 1 min to get response from MyRunnable.run()
	// If method execution is not finished in 1 min then it will skip sitetech for that document.  
	void callThread(String id,String link,DBCollection parsedDataForVPS)
	{
		ExecutorService executor;
		Future<?> future;		
		
		try
		{	
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	
				@Override
				public void run() 
				{  
					// Call run() to get technologies.
					new MyRunnable(id,link,parsedDataForVPS).run(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(60, TimeUnit.SECONDS);  //     <-- wait 60 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("SiteInfo interrupted for id="+id);
			}
		}catch (Exception e) 
		{    //     <-- possible error cases
			System.out.println("SiteInfo interrupted for id="+id);
		}
	}
	
	// This method will get current load average of CPU using "uptime" command.
	double[] getLoadAvg()
	{
		StringBuffer output = new StringBuffer();
		double loads[]=new double[3];
        Process p;
        try {
                p = Runtime.getRuntime().exec("uptime");	// execute "uptime" command on linux only.
                p.waitFor();
                BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
                while ((line = reader.readLine())!= null) {
                     output.append(line + "\n"); //Output of command
                }

        } catch (Exception e) 
        {
                e.printStackTrace();
        }
        String str=output.toString();
        str=str.split("load average[s:][: ]")[1];  //To get only load avg from command output
        System.out.println("Load Average= "+str);
        String allValues[]=str.split(",");
        loads[0]=Double.parseDouble(allValues[0].trim()); //last one Min
        loads[1]=Double.parseDouble(allValues[1].trim()); //last five Min
        loads[2]=Double.parseDouble(allValues[2].trim()); //last fifteen Min
       // System.out.println(lastOneMin+"**"+lastFiveMin+"**"+lastFifteenMin+"**");
        return loads;

	}

}

class MyRunnable 
{
	// hm is Map which stores fields of technologies.  
	LinkedHashMap<String, String> hm;
		
	
	
	List<String> textsToProcess = new ArrayList<String>();
	Document doc;
	StringBuilder stringBuilder;	
	
	// All categories of technologies.
	static String fields[]={"webServer","framework", "language", "advertisingNetworks", "analytics", "blogs", "build_ciSystems", "cacheTools", 
							"captchas", "cdn", "commentSystems", "cms", "controlSystems", "databaseManagers", "databases", "devTools",
							"documentManagementSystems",  "documentationTools", "ecommerce", "editors", "feedReaders", "fontScripts", "hostingPanels",
							"issueTrackers", "javascriptFrameworks", "javascriptGraphics","landingPageBuilders","lms","maps","marketingAutomation",
							"mediaServers","messageBoards","miscellaneous","mobileFrameworks","networkDevices","networkStorage","operatingSystems",
							"paymentProcessors","paywalls","photoGalleries", "printers","remoteAccess","richTextEditors","searchEngines","tagManagers","videoPlayers",
							"webFrameworks","webMail", "webServerExtensions","webCams", "widgets","wikis"};
	
	static String command;
	static File directory;
	static
	{
		try
		{
			// phantomjs driver.js gives JSON containing technologies.
			command = new File(".").getCanonicalPath() + "/siteTechInfo/phantomjs driver.js ";
			directory =  new File(new File(".").getCanonicalFile() + "/siteTechInfo");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	String line = null;
	DBCollection selectedCollection;
	final String link;
	String id;
	
	MyRunnable(String id,String link,DBCollection selectedCollection) 
	{
		this.id=id;
		this.link = link;
		this.selectedCollection=selectedCollection;
		hm = new LinkedHashMap<String, String>();

		for(String field:fields) // Set All DB fields value empty
		{
			hm.put(field, "");
		}

		stringBuilder = new StringBuilder();  //stringBuilder to hold JSON
	}


	public void run() 
	{			
	  try
	  {
		// process executes from the specified directory in the third param;
					
		Process p;
		BufferedReader bufferedReader;
				
		p = Runtime.getRuntime().exec(command+link, null, directory); //Execute command with link as a parameter
		
		bufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			
		while ((line = bufferedReader.readLine()) != null) 
		{				
			stringBuilder.append(line);
		}
		p.destroyForcibly();
		bufferedReader.close();

		final JSONObject obj;
		obj = new JSONObject(stringBuilder.toString()); //JSON given to JSON Object
		final JSONArray applications = obj.getJSONArray("applications"); //Extract applications[] from JSON Object
		final int arrayCount = applications.length();
		JSONObject company;
		JSONArray categories;
		String str;
		String choice;
		
		if(arrayCount > 0)
		{
			for (int i = 0; i < arrayCount; i++) 
			{
				company = applications.getJSONObject(i);
				categories = company.getJSONArray("categories"); // Extract categories[] from applications[] object array 
			
				for (int j = 0; j < categories.length(); j++) 
				{
					str="";
					choice="";
					
					str=" "+company.getString("name") +" "+ company.getString("version") + ","; // Concat technology name and version of each category
					
				    choice=categories.get(j).toString().toLowerCase();
					
				    switch(choice) //Check category of technology and then add to hm map
					{
						case "web-servers"					: hm.put("webServer", hm.get("webServer")+str); 
															  break; 
						case "frameworks" 					: hm.put("framework", hm.get("framework")+str); 
															  break;
						case "programming-languages"		: hm.put("language", hm.get("language")+str); 
															  break;
						case "programming language" 		: hm.put("language", hm.get("language")+str); 
															  break;
						case "advertising-networks" 		: hm.put("advertisingNetworks", hm.get("advertisingNetworks")+str); 
															  break;
						case "analytics"					: hm.put("analytics", hm.get("analytics")+str); 
															  break;
						case "blogs"						: hm.put("blogs", hm.get("blogs")+str); 
															  break;
						case "build/ci-systems"				: hm.put("build_ciSystems", hm.get("build_ciSystems")+str); 
															  break;
						case "cache-tools"					: hm.put("cacheTools", hm.get("cacheTools")+str); 
															  break;
						case "captchas"						: hm.put("captchas", hm.get("captchas")+str); 
															  break;
						case "cdn"							: hm.put("cdn", hm.get("cdn")+str); 
															  break;
						case "comment-systems"				: hm.put("commentSystems", hm.get("commentSystems")+str); 
															  break;
						case "cms"							: hm.put("cms", hm.get("cms")+str); 
															  break;
						case "control-systems"				: hm.put("controlSystems", hm.get("controlSystems")+str); 
															  break;
						case "database-managers"			: hm.put("databaseManagers", hm.get("databaseManagers")+str); 
															  break;
						case "databases"					: hm.put("databases", hm.get("databases")+str); 
															  break;
						case "dev-tools"					: hm.put("devTools", hm.get("devTools")+str); 
															  break;
						case "document-management-systems"	: hm.put("documentManagementSystems", hm.get("documentManagementSystems")+str);  
															  break;
						case "documentation-tools"			: hm.put("documentationTools", hm.get("documentationTools")+str); 
															  break;
						case "ecommerce"					: hm.put("ecommerce", hm.get("ecommerce")+str); 
															  break;
						case "editors"						: hm.put("editors", hm.get("editors")+str); 
															  break;
						case "feed-readers"					: hm.put("feedReaders", hm.get("feedReaders")+str); 
															  break;
						case "font-scripts"					: hm.put("fontScripts", hm.get("fontScripts")+str); 
															  break;
						case "hosting-panels"				: hm.put("hostingPanels", hm.get("hostingPanels")+str); 
															  break;	
						case "issue-trackers"				: hm.put("issueTrackers", hm.get("issueTrackers")+str); 
															  break;	
						case "javascript-frameworks"		: hm.put("javascriptFrameworks", hm.get("javascriptFrameworks")+str); 
															  break;	
						case "javascript-graphics"			: hm.put("javascriptGraphics", hm.get("javascriptGraphics")+str);
															  break;	
						case "landing-page-builders"		: hm.put("landingPageBuilders", hm.get("landingPageBuilders")+str);
															  break;	
						case "lms"							: hm.put("lms", hm.get("lms")+str);
															  break;	
						case "maps"							: hm.put("maps", hm.get("maps")+str);
															  break;	
						case "marketing-automation"			: hm.put("marketingAutomation", hm.get("marketingAutomation")+str);
															  break;	
						case "media-servers"				: hm.put("mediaServers", hm.get("mediaServers")+str);
															  break;	
						case "message-boards"				: hm.put("messageBoards", hm.get("messageBoards")+str);
															  break;							
						case "miscellaneous"				: hm.put("miscellaneous", hm.get("miscellaneous")+str);
															  break;	
						case "mobile-frameworks"			: hm.put("mobileFrameworks", hm.get("mobileFrameworks")+str);
															  break;	
						case "network-devices"				: hm.put("networkDevices", hm.get("networkDevices")+str);
															  break;	
						case "network-storage"				: hm.put("networkStorage", hm.get("networkStorage")+str);
															  break;	
						case "operating-systems"			: hm.put("operatingSystems", hm.get("operatingSystems")+str);
															  break;	
						case "payment-processors"			: hm.put("paymentProcessors", hm.get("paymentProcessors")+str);
															  break;	
						case "paywalls"						: hm.put("paywalls", hm.get("paywalls")+str);
															  break;	
						case "photo-galleries"				: hm.put("photoGalleries", hm.get("photoGalleries")+str);
															  break;	
						case "printers"						: hm.put("printers", hm.get("printers")+str);
														      break;	
						case "remote-access"				: hm.put("remoteAccess", hm.get("remoteAccess")+str);
															  break;	
						case "rich-text-editors"			: hm.put("richTextEditors", hm.get("richTextEditors")+str);
															  break;	
						case "search-engines"				: hm.put("searchEngines", hm.get("searchEngines")+str);
															  break;	
						case "tag-managers"					: hm.put("tagManagers", hm.get("tagManagers")+str);
															  break;	
						case "video-players"				: hm.put("videoPlayers", hm.get("videoPlayers")+str);
															  break;	
						case "web-frameworks"				: hm.put("webFrameworks", hm.get("webFrameworks")+str);
															  break;	
						case "web-mail"						: hm.put("webMail", hm.get("webMail")+str);
															  break;	
						case "web-server-extensions"		: hm.put("webServerExtensions", hm.get("webServerExtensions")+str);
															  break;	
						case "webcams"						: hm.put("webCams", hm.get("webCams")+str);
															  break;	
						case "widgets"						: hm.put("widgets", hm.get("widgets")+str);
															  break;	
						case "wikis"						: hm.put("wikis", hm.get("wikis")+str); 
						 									  break;	
						default								: hm.put(choice, str);
					}					
				}
			}
		
			//To remove Last Comma if value is present for field
			for(Map.Entry<String, String> v : hm.entrySet() ) 
			{	
				if(v.getValue().endsWith(",")) 
				{
					hm.put(v.getKey(),v.getValue().substring(0,v.getValue().length()-1));
				}
				//display+="\n"+v.getKey()+" ="+hm.get(v.getKey());
				//System.out.println("\n"+v.getKey()+" ="+hm.get(v.getKey()));
			}
			
			hm.put("sitetechStatus" , "true");
			// Add 52 fields to DB.
			selectedCollection.update(new BasicDBObject("link",link),new BasicDBObject("$set",new BasicDBObject(hm)));
			System.out.println("\n\n*****************************\n"+id+"  SiteTech Info Added. Status="+hm.get("sitetechStatus")+"\t"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date()))+"\n");
			hm.clear();
			hm=null;
		}	
		else	
		{
			// If JSON array is Empty. Sometimes not found becoz of slow internet connection or server is unavailable for time being. Retest such documents.
			selectedCollection.update(new BasicDBObject("link",link), new BasicDBObject().append("$set", new BasicDBObject("sitetechStatus", "notFound")));
			System.out.println("\n\n*****************************\n"+id+" Not Found. Json= "+stringBuilder+"\t"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
		}
	}
	catch(Exception e)
	{
		selectedCollection.update(new BasicDBObject("link",link), new BasicDBObject().append("$set", new BasicDBObject("sitetechStatus", "exception")));
		System.out.println("\n\n*****************************\n"+id+" Exception Occurred. Json= "+stringBuilder+"\t"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
	}				
}

}

